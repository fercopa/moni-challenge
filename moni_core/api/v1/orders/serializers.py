from rest_framework import serializers
from orders.models import Order


class OrderSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    status = serializers.CharField(required=False, read_only=True)

    class Meta:
        model = Order
        fields = ("id", "amount", "status")
