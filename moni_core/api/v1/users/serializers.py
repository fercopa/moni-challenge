from django.db import transaction

from rest_framework import serializers

from users.models import User
from orders.models import Order
from orders.tasks import task_process_order
from api.v1.orders.serializers import OrderSerializer


class UserSerializer(serializers.ModelSerializer):
    orders = OrderSerializer(many=True)

    class Meta:
        model = User
        fields = (
            "id",
            "dni",
            "first_name",
            "last_name",
            "gender",
            "email",
            "orders",
        )

    def create(self, validated_data):
        with transaction.atomic():
            orders = validated_data.pop("orders", [])
            user = User.objects.filter(dni=validated_data.get("dni")).first()
            if user is None:
                user = User(**validated_data)
                user.save()
            instances = self.create_orders(orders, user)
        self.process_orders(instances)
        return user

    @staticmethod
    def create_orders(orders_data, user):
        orders = []
        for order_data in orders_data:
            order = Order(user=user, **order_data)
            order.save()
            orders.append(order)
        return orders

    @staticmethod
    def process_orders(orders):
        for order in orders:
            task_process_order.delay(order.user_id)

    def update(self, instance, validated_data):
        with transaction.atomic():
            instance.dni = validated_data.get("dni", instance.dni)
            instance.first_name = validated_data.get("first_name", instance.first_name)
            instance.last_name = validated_data.get("last_name", instance.last_name)
            instance.gender = validated_data.get("gender", instance.gender)
            instance.email = validated_data.get("email", instance.email)
            instance.save()
            orders_data = validated_data.get("orders", []) or []
            orders = self.update_orders(orders_data, instance)
        self.process_orders(orders)
        return instance

    @staticmethod
    def update_orders(orders_data, user):
        orders = []
        for order_data in orders_data:
            order_id = order_data.get("id")
            if order_id:
                order = Order.objects.get(id=order_id)
                order.amount = order_data.get("amount", order.amount)
                order.status = order_data.get("status", order.status)
                order.save()
            else:
                order = Order(user=user, **order_data)
                order.save()
            orders.append(order)
        return orders
