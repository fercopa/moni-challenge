from django.urls import path, include
from rest_framework.routers import DefaultRouter

from api.v1.views import OrderView, UserView


router = DefaultRouter()
router.register("users", UserView, basename="user")
router.register("orders", OrderView, basename="order")

urlpatterns = [
    path("", include(router.urls)),
]
