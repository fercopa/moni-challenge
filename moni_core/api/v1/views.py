from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated, AllowAny

from orders.models import Order
from users.models import User
from api.v1.orders.serializers import OrderSerializer
from api.v1.users.serializers import UserSerializer


class OrderView(ModelViewSet):
    serializer_class = OrderSerializer
    permission_classes = [IsAuthenticated]
    queryset = Order.objects.all()


class UserView(ModelViewSet):
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return User.objects.all().prefetch_related("orders").order_by("-created_at")

    def get_permissions(self):
        if self.action == 'create':
            self.permission_classes = [AllowAny]
        return super().get_permissions()

    def perform_destroy(self, instance):
        for order in instance.orders.all():
            order.delete()
        super().perform_destroy(instance)
