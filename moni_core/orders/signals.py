from django.db.models.signals import post_save
from django.dispatch import receiver

from orders.models import Order
from orders.utils import record_order_history


@receiver(post_save, sender=Order)
def create_history(sender, instance, created, **kwargs):
    if instance.is_dirty() or created:
        record_order_history(instance)
