from django.utils.translation import gettext_lazy as _
from django.db import models

from dirtyfields import DirtyFieldsMixin

from users.models import User
from common.managers import ActiveManager


class Order(models.Model, DirtyFieldsMixin):
    class Status(models.TextChoices):
        PENDING = "Pending", _("Pending")
        APPROVED = "Approved", _("Approved")
        DENIED = "Denied", _("Denied")

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="orders")
    amount = models.FloatField()
    created_at = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=20, choices=Status.choices, default=Status.PENDING)
    active = models.BooleanField(default=True)
    objects = ActiveManager()
    objects_all = models.Manager()

    def delete(self):
        self.active = False
        self.save()


class OrderHistory(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    previous_amount = models.FloatField(null=True)
    new_amount = models.FloatField()
    previous_status = models.CharField(max_length=20, null=True)
    new_status = models.CharField(max_length=20)
    previous_active = models.BooleanField(null=True)
    new_active = models.BooleanField()
