# Generated by Django 4.2.3 on 2023-07-06 22:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
        ('orders', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='orders', to='users.user'),
        ),
        migrations.AlterField(
            model_name='orderhistory',
            name='previous_active',
            field=models.BooleanField(null=True),
        ),
        migrations.AlterField(
            model_name='orderhistory',
            name='previous_amount',
            field=models.FloatField(null=True),
        ),
        migrations.AlterField(
            model_name='orderhistory',
            name='previous_status',
            field=models.CharField(max_length=20, null=True),
        ),
    ]
