import os
import requests
from orders.models import OrderHistory, Order
from orders.exceptions import RequestOrderError


def record_order_history(order):
    last_order_history = OrderHistory.objects.filter(order=order).order_by("timestamp").last()
    if last_order_history:
        OrderHistory.objects.create(
            order=order,
            previous_amount=last_order_history.new_amount,
            new_amount=order.amount,
            previous_status=last_order_history.new_status,
            new_status=order.status,
            previous_active=last_order_history.new_active,
            new_active=order.active,
        )
    else:
        OrderHistory.objects.create(
            order=order,
            previous_amount=None,
            new_amount=order.amount,
            previous_status=None,
            new_status=order.status,
            previous_active=None,
            new_active=order.active,
        )


class MoniApi:
    URL = os.getenv("MONI_API")
    HEADERS = {"Autorization": os.getenv("CREDENTIAL")}

    def is_approved(self, dni):
        endpoint = f"{self.URL}/{dni}"
        response = requests.get(endpoint, headers=self.HEADERS)
        response.raise_for_status()
        data = response.json()
        return data.get("status") == "approve"


def process_order(user_id):
    moni_api = MoniApi()
    try:
        order = (
            Order.objects
            .select_related("user")
            .filter(user_id=user_id)
            .exclude(status=Order.Status.APPROVED)
            .last()
        )
        order.status = Order.Status.APPROVED if moni_api.is_approved(order.user.dni) else Order.Status.DENIED
        order.save()
    except (
        requests.RequestException,
        requests.ConnectionError,
        requests.HTTPError,
        requests.TooManyRedirects,
        requests.ConnectTimeout,
        requests.ReadTimeout,
        requests.Timeout,
        requests.JSONDecodeError,
    ):
        raise RequestOrderError("There is an external error")

    except Order.DoesNotExist:
        return
