from celery import shared_task

from orders.exceptions import RequestOrderError
from orders.utils import process_order


@shared_task(bind=True, autoretry_for=(RequestOrderError,), retry_backoff=True, retry_kwargs={'max_retries': 5})
def task_process_order(self, user_id):
    process_order(user_id)
