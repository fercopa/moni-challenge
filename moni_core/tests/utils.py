import json

from django.contrib.auth import get_user_model
from rest_framework.test import APIClient

User = get_user_model()

JSON_API_CONTENT_TYPE = "application/json"
JSON_API_ACCEPT_HEADER = "application/json"


def create_user_admin():
    admin, _ = User.objects.get_or_create(
        username="superuser",
        email="superuser@mail.com",
        is_staff=True,
        is_active=True,
        is_superuser=True)
    admin.set_password("superuserpassword")
    admin.save()
    return admin


def http_attributes(url: str, headers: dict = None, user_logged: User = None):
    url = url + '/' if not url.endswith('/') else url
    client = get_authenticated_client(user_logged)
    headers = {'HTTP_ACCEPT': JSON_API_ACCEPT_HEADER} if headers is None else headers

    return client, url, headers


def get_authenticated_client(user=None):
    user = create_user_admin() if user is None else user
    client = APIClient()
    client.force_authenticate(user=user)
    return client


def get(url, params=None, headers=None, user_logged=None, ):
    client, _, headers = http_attributes(url, headers, user_logged)
    return client.get(url, params=params, **headers)


def post(url, data=None, params=None, content_type=JSON_API_CONTENT_TYPE, headers=None, user_logged=None):
    client, url, headers = http_attributes(url, headers, user_logged)
    return client.post(url, data=json.dumps(data), content_type=content_type, params=params, **headers)


def put(url, data={}, content_type=JSON_API_CONTENT_TYPE, headers=None, user_logged=None, **kwargs):
    client, _, headers = http_attributes(url, headers, user_logged)
    return client.put(url, data=json.dumps(data), content_type=content_type, **headers, **kwargs)


def patch(url, data=None, content_type=JSON_API_CONTENT_TYPE, headers=None, user_logged=None):
    client, url, headers = http_attributes(url, headers, user_logged)
    return client.patch(url, data=json.dumps(data), content_type=content_type, **headers)


def delete(url, data=None, content_type=JSON_API_CONTENT_TYPE, headers=None, user_logged=None):
    client, url, headers = http_attributes(url, headers, user_logged)
    return client.delete(url, data=json.dumps(data), content_type=content_type, **headers)
