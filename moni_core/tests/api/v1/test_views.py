import pytest
from unittest.mock import patch

from django.urls import reverse

from rest_framework import status

from users.models import User
from orders.models import Order
from api.v1.users.serializers import UserSerializer
from tests.utils import post, put, delete


@pytest.mark.django_db
def test_create_user():
    total_users = User.objects.count()
    url = reverse("user-list")
    data = {
        "orders": [
            {"amount": 23333}
        ],
        "dni": 29653246,
        "first_name": "Fernando",
        "last_name": "Copa",
        "gender": "Male",
        "email": "yo@mail.com"
    }
    with patch.object(UserSerializer, "process_orders", return_value=None) as m:
        response = post(url, data=data)
        assert response.status_code == status.HTTP_201_CREATED
        assert User.objects.count() > total_users
        m.assert_called()


@pytest.mark.django_db
def test_update_user(user):
    previous_last_name = user.last_name
    url = reverse("user-detail", kwargs={"pk": user.id})
    data = {
        "orders": [
            {"amount": 23333}
        ],
        "dni": 29653246,
        "first_name": "Fernando",
        "last_name": "Copa",
        "gender": "Male",
        "email": "yo@mail.com"
    }
    with patch.object(UserSerializer, "process_orders", return_value=None) as m:
        response = put(url, data=data)
        assert response.status_code == status.HTTP_200_OK
        user.refresh_from_db()
        assert user.last_name != previous_last_name
        m.assert_called()


@pytest.mark.django_db
def test_delete_user(user):
    total_users = User.objects.count()
    url = reverse("user-detail", kwargs={"pk": user.id})
    response = delete(url)
    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert User.objects.count() < total_users
    assert Order.objects.filter(user_id=user.id).count() == 0
