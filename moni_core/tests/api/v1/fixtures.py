import pytest
from tests.api.v1.factories import UserFactory, OrderFactory


@pytest.fixture
def user():
    user = UserFactory()
    OrderFactory(user=user)
    return user
