import factory
from users.models import User
from orders.models import Order


class UserFactory(factory.django.DjangoModelFactory):
    dni = factory.Sequence(lambda x: x + 1)
    first_name = factory.faker.Faker("first_name")
    last_name = factory.faker.Faker("last_name")
    gender = "Male"
    email = factory.faker.Faker("email")

    class Meta:
        model = User


class OrderFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    amount = factory.Sequence(lambda x: x + 1)

    class Meta:
        model = Order
