from django.db import models
from django.utils.translation import gettext_lazy as _

from common.managers import ActiveManager


class User(models.Model):
    class Gender(models.TextChoices):
        MALE = "Male", _("Male")
        FEMALE = "Female", _("Female")
        NON_BINARY = "Non_Binary", _("Non Binary")
        OTHER = "Other", _("Other")
        PREFER_NOT_TO_SAY = "Prefer_not_to_say", _("Prefer not to say")

    dni = models.PositiveIntegerField(db_index=True, unique=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    gender = models.CharField(max_length=20, choices=Gender.choices)
    email = models.EmailField(unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=True)
    objects = ActiveManager()
    objects_all = models.Manager()

    def delete(self):
        self.active = False
        self.save()
