# Generated by Django 4.2.3 on 2023-07-06 16:32

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dni', models.PositiveIntegerField(db_index=True)),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('gender', models.CharField(choices=[('Male', 'Male'), ('Female', 'Female'), ('Non_Binary', 'Non Binary'), ('Other', 'Other'), ('Prefer_not_to_say', 'Prefer not to say')], max_length=20)),
                ('email', models.EmailField(max_length=254)),
            ],
        ),
    ]
