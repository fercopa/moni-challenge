import { createContext, useState, useEffect} from "react"
import jwt_decode from "jwt-decode"
import {useNavigate} from "react-router-dom"


const AuthContext = createContext()

export default AuthContext

export const AuthProvider = ({children}) => {
  let [authToken, setAuthToken] = useState(() => localStorage.getItem("authToken")? JSON.parse(localStorage.getItem("authToken")): null)
  let [user, setUser] = useState(() => localStorage.getItem("authToken")? JSON.parse(localStorage.getItem("authToken")): null)
  let [loading, setLoading] = useState(true)
  const navigate = useNavigate()

  let loginUser = async (userData) => {
    let response = await fetch("http://127.0.0.1:8000/api/token/", {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify({"username": userData.username, "password": userData.password})
    })
    let data = await response.json()
    if (response.status === 200) {
      setAuthToken(data)
      setUser(jwt_decode(data.access))
      localStorage.setItem("authToken", JSON.stringify(data))
    }
    return {status: response.status, data: data}
  }

  let logoutUser = () => {
    setAuthToken(null)
    setUser(null)
    localStorage.removeItem("authToken")
    navigate("/login")
  }

  let updateToken = async () => {
    let response = await fetch("http://127.0.0.1:8000/api/token/refresh/", {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify({"refresh": authToken?.refresh})
    })
    let data = await response.json()
    if (response.status === 200) {
      setAuthToken(data)
      setUser(jwt_decode(data.access))
      localStorage.setItem("authToken", JSON.stringify(data))
    }
    if (loading) { setLoading(false) }
  }

  let contextData = {
    user: user,
    authToken: authToken,
    loginUser: loginUser,
    logoutUser: logoutUser
  }

  useEffect(() => {
    if (loading) { updateToken() }
    let fiveMin = 1000 * 60 * 5
    let interval = setInterval(() => {
      if (authToken) { updateToken() }
    }, fiveMin)
    return () => clearInterval(interval)
  }, [authToken, loading])
  
  return (
    <AuthContext.Provider value={contextData}>
      {loading ? null : children}
    </AuthContext.Provider>
  )
}
