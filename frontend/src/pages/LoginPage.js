import React, {useContext} from "react"
import AuthContext from "../context/AuthContext"
import { Button, Form, Input, message } from 'antd'
import {useNavigate} from "react-router-dom"

const LoginPage = () => {
  let {loginUser} = useContext(AuthContext)
  const [messageApi, contextHolder] = message.useMessage();
  const navigate = useNavigate()

  const onFinish = (values) => {
    let data = loginUser(values)
    data.then(function(result){
      if (result.status !== 200){
        messageApi.open({
          type: 'error',
          content: 'Your login info is not correct. Please check your email and password and try again',
        })
      } else {
        navigate("/users")
      }
    })
  };

  return (
    <div>
      {contextHolder}
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        style={{
          maxWidth: 600,
        }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item
          label="Username"
          name="username"
          rules={[
            {
              required: true,
              message: 'Please input your username!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  )
}
export default LoginPage
