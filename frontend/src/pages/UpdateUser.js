import React, {useEffect, useContext} from 'react'
import {useParams, useNavigate} from "react-router-dom"
import AuthContext from "../context/AuthContext"
import { Button, Form, Input, Select, InputNumber, message } from 'antd'

const UpdateUser = () => {
  let {id} = useParams()
  let {authToken} = useContext(AuthContext)
  const [messageApi, contextHolder] = message.useMessage();
  const [form] = Form.useForm()
  const navigate = useNavigate()
  useEffect(() => {
    fetch("http://127.0.0.1:8000/api/v1/users/" + id,{
      method: "GET",
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer " + String(authToken.access)
      },
    }).then(function(res) {
        if(res.status == 200){
          return res.json();
        } else {
          navigate("/login")
        }
      }).then((resp) => {
        form.setFieldsValue({
          dni: resp.dni,
          first_name: resp.first_name,
          last_name: resp.last_name,
          gender: resp.gender,
          email: resp.email,
          amount: resp.orders[0].amount,
        });
      })
  }, [])

  const onFinish = async (values) => {
    let response = await fetch(`http://localhost:8000/api/v1/users/${id}/`, {
      method: "PUT",
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer " + String(authToken.access)
      },
      body: JSON.stringify({
        "dni": values.dni,
        "first_name": values.first_name,
        "last_name": values.last_name,
        "gender": values.gender,
        "email": values.email,
        "orders": [{
          "amount": values.amount,
        }]
      })

    })
    let data = await response.json()
    if (response.status === 200) {
      messageApi.open({
        type: 'success',
        content: 'Updated successfully',
      });
    } else {
      Object.values(data).forEach(val => messageApi.open({ type: 'error', content: val, duration:5}))
    }
  };

  return (
    <>
    {contextHolder}
      <Form
        form={form}
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        style={{
          maxWidth: 600,
        }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item
          label="DNI"
          name="dni"
          rules={[
            {
              required: true,
              message: 'Please input your DNI!',
            },
          ]}
        >
          <Input placeholder="Ex: 40333222" />
        </Form.Item>

        <Form.Item
          label="First name"
          name="first_name"
          rules={[
            {
              required: true,
              message: 'Please input your first name!',
            },
          ]}
        >
          <Input placeholder="Ex: Bob "/>
        </Form.Item>

        <Form.Item
          label="Last name"
          name="last_name"
          rules={[
            {
              required: true,
              message: 'Please input your last name!',
            },
          ]}
        >
          <Input placeholder="Ex: Falcioni"/>
        </Form.Item>

        <Form.Item
          label="Gender"
          name="gender"
        >
          <Select
            options={[
              {
                value: 'Male',
                label: 'Male',
              },
              {
                value: 'Female',
                label: 'Female',
              },
              {
                value: 'Non_Binary',
                label: 'Non Binary',
              },
              {
                value: 'Other',
                label: 'Other',
              },
              {
                value: 'Prefer_not_to_say',
                label: 'Prefer not to say',
              },
            ]}
          />
        </Form.Item>

        <Form.Item
          label="Email"
          name="email"
          rules={[
            {
              required: true,
              type: "email",
              message: 'Please input your email!',
            },
          ]}
        >
          <Input placeholder="Ex: admin@admin.com"/>
        </Form.Item>

        <Form.Item
          label="Amount"
          name="amount"
          rules={[
            {
              required: true,
              message: 'Please input the amount. Min 1!',
            },
          ]}
        >
          <InputNumber min={1} style={{ width: 200 }} placeholder="Min value 1"/>
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
      </>
  )
}

export default UpdateUser
