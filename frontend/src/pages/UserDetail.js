import React, {useState, useEffect, useContext} from 'react'
import {useParams} from "react-router-dom"
import { Card } from 'antd'
import AuthContext from "../context/AuthContext"

const UserDetail = () => {
  let {id} = useParams()
    let [user, setUser] = useState([])
    let {authToken, logoutUser} = useContext(AuthContext)
    useEffect(() => {
        getUser()
    }, [])
    let getUser = async () => {
        let response = await fetch("http://127.0.0.1:8000/api/v1/users/" + id,{
            method: "GET",
            headers: {
                "Content-type": "application/json",
                "Authorization": "Bearer " + String(authToken.access)
            },
        })
        let data = await response.json()
        if (response.status === 200) {
            setUser(data)
        } else if (response.status === 401) {
            logoutUser()
        }
    }
  return (
  <Card
    title={`${user.last_name} ${user.first_name}`}
    bordered={false}
    style={{
      width: 300,
    }}
  >
    <p>DNI: {user.dni}</p>
    <p>First name: {user.first_name}</p>
    <p>Last name: {user.last_name}</p>
    <p>Gender: {user.gender}</p>
    <p>Email: {user.email}</p>
  </Card>
  )
}

export default UserDetail
