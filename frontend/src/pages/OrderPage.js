import React from "react"
import { Button, Form, Input, Select, InputNumber, message } from 'antd'

const OrderPage = ({initial = {gender: "Prefer_not_to_say"}}) => {
  const [messageApi, contextHolder] = message.useMessage();
  const [form] = Form.useForm()
  const initValues = {
    id: initial.id,
    dni: initial.dni,
    first_name: initial.first_name,
    last_name: initial.last_name,
    gender: initial.gender,
    email: initial.email,
    amount: initial.orders?initial.orders[0].amount: null,

  }

  const onFinish = async (values) => {
    let response = await fetch("http://localhost:8000/api/v1/users/", {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify({
        "dni": values.dni,
        "first_name": values.first_name,
        "last_name": values.last_name,
        "gender": values.gender,
        "email": values.email,
        "orders": [{
          "amount": values.amount,
        }]
      })

    })
    let data = await response.json()
    if (response.status === 201) {
      messageApi.open({
        type: 'success',
        content: 'Request sent successfully',
      });
      form.resetFields()
    } else {
      Object.values(data).forEach(val => messageApi.open({ type: 'error', content: val, duration:5}))
    }
  };

  return (
    <div>
      {contextHolder}
      <Form
        form={form}
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        style={{
          maxWidth: 600,
        }}
        onFinish={onFinish}
        initialValues={initValues}
        autoComplete="off"
      >
        <Form.Item
          label="DNI"
          name="dni"
          rules={[
            {
              required: true,
              message: 'Please input your DNI!',
            },
          ]}
        >
          <Input placeholder="Ex: 40333222"/>
        </Form.Item>

        <Form.Item
          label="First name"
          name="first_name"
          rules={[
            {
              required: true,
              message: 'Please input your first name!',
            },
          ]}
        >
          <Input placeholder="Ex: Bob "/>
        </Form.Item>

        <Form.Item
          label="Last name"
          name="last_name"
          rules={[
            {
              required: true,
              message: 'Please input your last name!',
            },
          ]}
        >
          <Input placeholder="Ex: Falcioni"/>
        </Form.Item>

        <Form.Item
          label="Gender"
          name="gender"
        >
          <Select
            options={[
              {
                value: 'Male',
                label: 'Male',
              },
              {
                value: 'Female',
                label: 'Female',
              },
              {
                value: 'Non_Binary',
                label: 'Non Binary',
              },
              {
                value: 'Other',
                label: 'Other',
              },
              {
                value: 'Prefer_not_to_say',
                label: 'Prefer not to say',
              },
            ]}
          />
        </Form.Item>

        <Form.Item
          label="Email"
          name="email"
          rules={[
            {
              required: true,
              type: "email",
              message: 'Please input your email!',
            },
          ]}
        >
          <Input placeholder="Ex: admin@admin.com"/>
        </Form.Item>

        <Form.Item
          label="Amount"
          name="amount"
          rules={[
            {
              required: true,
              message: 'Please input the amount. Min 1!',
            },
          ]}
        >
          <InputNumber min={1} style={{ width: 200 }} placeholder="Min value 1"/>
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  )
}
export default OrderPage
