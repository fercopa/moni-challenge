import './App.css';
import 'antd/dist/reset.css';
import {BrowserRouter, Routes, Route, Navigate} from "react-router-dom"
import PrivateRoute from "./utils/PrivateRoute"
import {AuthProvider} from "./context/AuthContext"
import LoginPage from "./pages/LoginPage"
import OrderPage from "./pages/OrderPage"
import UserList from "./components/UserList"
import UserDetail from "./pages/UserDetail"
import UpdateUser from "./pages/UpdateUser"
import AppLayaut from "./components/AppLayaut"



function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <AuthProvider>
          <Routes>
            <Route element={<Navigate to="/orders" />} path="/" exact />
            <Route element={<AppLayaut><OrderPage /></AppLayaut>} path="/orders" />
            <Route element={<AppLayaut><LoginPage /></AppLayaut>} path="/login" />
            <Route element={<PrivateRoute />}>
              <Route element={<AppLayaut><UserList /></AppLayaut>} path="/users" />
              <Route element={<AppLayaut><UserDetail /></AppLayaut>} path="/users/detail/:id" />
              <Route element={<AppLayaut><UpdateUser /></AppLayaut>} path="/users/detail/:id/edit" />
            </Route>
          </Routes>
        </AuthProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
