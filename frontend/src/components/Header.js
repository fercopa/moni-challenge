import React from "react"
import { Typography, Space } from 'antd';

const { Title } = Typography;

const MoniHeader = () => {
  return (
    <Space>
      <Title level={3}>Welcome to Moni</Title>
    </Space>
  )
}

export default MoniHeader
