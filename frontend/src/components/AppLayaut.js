import Sidebar from "./Sidebar"
import MoniHeader from "./Header"
import { Layout } from 'antd'

const { Header, Content, Footer} = Layout;

const AppLayaut = ({children}) => {
  return (
    <Layout>
      <Sidebar />
      <Layout>
        <Header style={{ padding: 0, background: "white" }}>
          <MoniHeader />
        </Header>
        <Content style={{ margin: '24px 16px 0' }}>
          <div style={{ padding: 24, minHeight: 360 }}>
            {children}
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>©2023 Created by Fernando Copa</Footer>
      </Layout>
    </Layout>
  )
}
export default AppLayaut
