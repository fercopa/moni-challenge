import React, {useContext} from 'react';
import {Link, useLocation} from "react-router-dom"
import { TeamOutlined, DollarOutlined, LoginOutlined, LogoutOutlined } from '@ant-design/icons';
import { Layout, Menu } from 'antd';
import AuthContext from "../context/AuthContext"


const { Sider } = Layout;


const Sidebar = () => {
  let {user, logoutUser} = useContext(AuthContext)
  const location = useLocation()
  const onClick = (e) => {
    if (e.key === "/logout"){
      logoutUser()
    }
  };
  return (
      <Sider
        breakpoint="lg"
        collapsedWidth="0"
      >
        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={[location.pathname]}
          onClick={onClick}
        >
        {user?
        <Menu.Item key="/users">
            <TeamOutlined />
            <span>Users</span>
            <Link to="/users" />
          </Menu.Item>: null
        }
          <Menu.Item key="/orders">
            <DollarOutlined />
            <span>Request Loan</span>
            <Link to="/orders" />
          </Menu.Item>
          {user? <Menu.Item key="/logout"> <LogoutOutlined /> <span>Logout</span></Menu.Item>: <Menu.Item key="/login"> <LoginOutlined /> <span>Login</span><Link to="/login" /> </Menu.Item>}
        </Menu>
      </Sider>
  );
}

export default Sidebar
