import React, {useState, useEffect, useContext} from 'react'
import { Space, Table, Modal, message } from 'antd'
import {Link} from "react-router-dom"
import AuthContext from "../context/AuthContext"
import {DeleteOutlined, EditOutlined, EyeOutlined} from "@ant-design/icons"


const UserList = () => {
  const [messageApi, contextHolder] = message.useMessage();
  const handleOk = async (record) => {
    Modal.confirm({
      title:"Are you sure, you want to delete this user?",
      onOk: async () => {
        let response = await fetch(`http://localhost:8000/api/v1/users/${record.id}/`, {
          method: "DELETE",
          headers: {
            "Content-type": "application/json",
            "Authorization": "Bearer " + String(authToken.access)
          },
          body: JSON.stringify({})

        })
        if (response.status === 204) {
          messageApi.open({
            type: 'success',
            content: 'User deleted successfully',
          });
        }
        setUsers((pre) => {
          return pre.filter((user) => user.id !== record.id)
        })
      }
    })
  };
  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      render: (text) => <span>{text}</span>,
    },
    {
      title: 'User',
      dataIndex: 'user',
      key: 'user',
      render: (text) => <span>{text}</span>,
    },
    {
      title: 'Amount',
      dataIndex: 'amount',
      key: 'amount',
      render: (text) => <span>{text}</span>,
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: (text) => <span>{text}</span>,
    },
    {
      title: 'Action',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <Link to={`/users/detail/${record.key}`}>
            <EyeOutlined />
          </Link>
          <Link to={`/users/detail/${record.key}/edit`}>
            <EditOutlined />
          </Link>
          <DeleteOutlined onClick={() => {handleOk(record)}} style={{color: "red", marginLeft: 12}} />
        </Space>
      ),
    },
  ];
  let [users, setUsers] = useState([])
  let {authToken, logoutUser} = useContext(AuthContext)
  useEffect(() => {
    getUsers()
  }, [])
  let getUsers = async () => {
    let response = await fetch("http://127.0.0.1:8000/api/v1/users/",{
      method: "GET",
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer " + String(authToken.access)
      },
    })
    let data = await response.json()
    if (response.status === 200) {
      const userList = data.map(user =>(
        {
          id: user.id,
          key: user.id,
          user: user.last_name + " " + user.first_name,
          amount:user.orders?user.orders[0].amount:null,
          status:user.orders?user.orders[0].status:null
        }
      )
      )
      setUsers(userList)
    } else if (response.status === 401) {
      logoutUser()
    }
  }
  return(
    <>
    {contextHolder}
    <Table columns={columns} dataSource={users} />
    </>
  )
};

export default UserList;
