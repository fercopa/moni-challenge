# moni-challenge



## Documentación
Se encuentra detallado en [Wiki](https://gitlab.com/fercopa/moni-challenge/-/wikis/home)


## Cómo usar

Clonar el repositorio

```
git clone https://gitlab.com/fercopa/moni-challenge.git myproj

```

## Para correr los servcios del Core
Existe un archivo docker-compose para el Core lo cual levanta los servicios necesarios.

Existe un archivo de variables de entorno que están definidas en el `.env` si bien no es recomendado subir estos tipos de datos a un repositorio, es para este caso nada más. En AWS y GCP existen servicios para delcarar variables de entornos y es allí donde se debería configurar.

```
cd myproj
docker-compose up --build
```

## Administrador

Cuando se levanta con docker ya crea un super usuario que es el Admin. Las credenciales son username **admin** y password **admin**
Esto también se puede correr con el siguiente comando. Ya que creé un Command para ello

```
python manage.py initadmins
```

## Correr los servicios del Front

No cree un dokcer compose por falta de tiempo, pero se puede levantar desde su carpeta **../frontend**

```
cd ../frontend
npm install
npm start
```

Luego navegar a `http://localhost:3000/login`
